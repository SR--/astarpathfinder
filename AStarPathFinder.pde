// A* path search with Euclidean distance heuristics
// using Processign 2.2.1
// and Path_Finder library

// Create map form 2 images, the first is a grey-scale image 
// and nodes are created in non-black areas with edges
// created between adjacent nodes. The cost of traversing 
// and edge depends on the level of grey-scale under the
// 'from' node's position.
// The second image is used as the visible image for 
// the map. The grey-scale image interprets
// the map image.

// Draw several sequential paths by repeatedly dragging the mouse

// TODO: Implement graph-making from Kinect data?

// TODO: Consider 3D implementations
// http://theory.stanford.edu/~amitp/GameProgramming/MapRepresentations.html

// NOTE: 3D v01: project nodes down to the terrain
// NOTE: 3D v02: implement a voxel grid
// NOTE: 3D v03: distribute note on a 3D mesh, e.g. as a force-directed graph 

// simplifying the colors (indexed, not too many)
// generates fewer number of edges and is much faster

import pathfinder.*;


Graph gs = new Graph();

PImage graphImage; // visible image
PImage costImg; // cost image
int start, end;

int numTilesX, numTilesY;

GraphNode[] gNodes, p;
GraphEdge[] gEdges, exploredEdges;
// Pathfinder algorithm
IGraphSearch pathFinder;
// Used to indicate the start and end nodes as selected by the user.
GraphNode startNode, endNode;

ArrayList<GraphNode[]> paths; // store paths
ArrayList<Integer> pathColor; // store path colors

long time; // used for performance stats
boolean ready = false;

// ------------------------------------------------------------
// SETUP

void setup(){
  size(640, 640);
  cursor(CROSS);
  smooth();
  ellipseMode(CENTER);

  graphImage = loadImage("map1a.png");
  costImg = loadImage("map1b.png");

  gs = new Graph();
  numTilesX = numTilesY = 40;
  makeGraphFromBWimage(gs, graphImage, costImg, numTilesX, numTilesY, true);

  // Get arrays of nodes and edges
  gNodes =  gs.getNodeArray();
  gEdges = gs.getAllEdgeArray();

  // Create a path finder object
  pathFinder = makePathFinder(gs);

  paths = new ArrayList<GraphNode[]>();
  pathColor = new ArrayList<Integer>();
}

// ------------------------------------------------------------
// DRAW

void draw(){
  background(0);
  display();
}

// ------------------------------------------------------------
// FUNCTIONS

IGraphSearch makePathFinder(Graph graph){
  IGraphSearch pf = null;
  float f = 2.0f;
  pf = new GraphSearch_Astar(gs, new AshCrowFlight(f));
  return pf;
}

GraphNode[] usePathFinder(IGraphSearch pf){
  time = System.nanoTime();
  pf.search(start, end, true);
  time = System.nanoTime() - time;
  p = pf.getRoute();
  exploredEdges = pf.getExaminedEdges();

  showStats();

  return p;
}

// Visualise the algorithm and path

void chooseRoute() {
  stroke(255, 0, 0);
  strokeWeight(1.5f);
  if(endNode != null)
    line(startNode.xf(), startNode.yf(), endNode.xf(), endNode.yf()); // if end node selected, draw route to it
  else
    line(startNode.xf(), startNode.yf(), mouseX, mouseY); // if end node has not been selected, draw to the mosue pos
}


// Display search analysis data
void showStats() {
  println("No. edges examined: " + exploredEdges.length);
  println("Analysis time: " + (time * 0.000001f));
  println();
}

// Graph drawing functions
void drawNodes(){
  pushStyle();
  noStroke();
  fill(255);
  for(GraphNode node : gNodes)
    ellipse(node.xf(), node.yf(), 2, 2);
  popStyle();
}

void drawEdges(GraphEdge[] edges, int lineCol, float sWeight){
  if(edges != null){
    pushStyle();
    noFill();
    stroke(lineCol);
    strokeWeight(sWeight);
    for(GraphEdge ge : edges)
      line(ge.from().xf(), ge.from().yf(), ge.to().xf(), ge.to().yf()); 
    popStyle();
  }
}

void drawRoute(GraphNode[] r, int lineCol, float sWeight){
  if(r.length >= 2){
    pushStyle();
    stroke(lineCol);
    strokeWeight(sWeight);
    noFill();
    for(int i = 1; i < r.length; i++)
      line(r[i-1].xf(), r[i-1].yf(), r[i].xf(), r[i].yf());
    // Route start node
    strokeWeight(0.0f);
    fill(0, 0, 255);
    ellipse(r[0].xf(), r[0].yf(), 5, 5);
    // Route end node
    fill(255, 0, 0);
    ellipse(r[r.length-1].xf(), r[r.length-1].yf(), 5, 5); 
    popStyle();
  } 
}

 /*
  * Create a tiled graph from an image.
  * This method will accept 1 or 2 images to create a tiled graph
  * (a 2D grid of nodes)
  */
void makeGraphFromBWimage(Graph g, PImage backImg, PImage costImg, int tilesX, int tilesY, boolean allowDiagonals){
  int dx = backImg.width / tilesX;
  int dy = backImg.height / tilesY;
  int sx = dx / 2, sy = dy / 2;
  // use deltaX to avoid horizontal wrap around edges
  int deltaX = tilesX + 1; // must be > tilesX

  float hCost = dx, vCost = dy, dCost = sqrt(dx*dx + dy*dy);
  float cost = 0;
  int px, py, nodeID, col;
  GraphNode aNode;

  py = sy;
  for(int y = 0; y < tilesY ; y++){
    nodeID = deltaX * y + deltaX;
    px = sx;
    for(int x = 0; x < tilesX; x++){
      // Calculate the cost
      if(costImg == null){
        col = backImg.get(px, py) & 0xFF;
        cost = 1;
      }
      else {
        col = costImg.get(px, py) & 0xFF;
        cost = 1.0f + (256.0f - col)/ 16.0f; 
      }
      // If col is not black then create the node and edges
      if(col != 0){
        aNode = new GraphNode(nodeID, px, py);
        g.addNode(aNode);
        if(x > 0){
          g.addEdge(nodeID, nodeID - 1, hCost * cost);
          if(allowDiagonals){
            g.addEdge(nodeID, nodeID - deltaX - 1, dCost * cost);
            g.addEdge(nodeID, nodeID + deltaX - 1, dCost * cost);
          }
        }
        if(x < tilesX -1){
          g.addEdge(nodeID, nodeID + 1, hCost * cost);
          if(allowDiagonals){
            g.addEdge(nodeID, nodeID - deltaX + 1, dCost * cost);
            g.addEdge(nodeID, nodeID + deltaX + 1, dCost * cost);
          }
        }
        if(y > 0)
          g.addEdge(nodeID, nodeID - deltaX, vCost * cost);
        if(y < tilesY - 1)
          g.addEdge(nodeID, nodeID + deltaX, vCost * cost);
      }
      px += dx;
      nodeID++;
    }
    py += dy;
  }
}

// Display the results
void display() {
  if(graphImage != null)
    image(graphImage, 0, 0);

  drawEdges(exploredEdges, color(255, 255, 255, 100), 1.0f);
  drawNodes();
    if(ready)
      for(int i = 0; i < paths.size(); i++)
        drawRoute(paths.get(i), pathColor.get(i), 2.5f); // draw all ready paths, use new color for each

  // show mouse-controlled start/end selection
  // can be rewritten to suit design
  if(mousePressed == true) {
    if(startNode != null)
      chooseRoute();
  }
}

// Initialisation
// TODO: Replace with values driven by design
public void mousePressed(){
  noCursor();
  if (paths.isEmpty()) {
    startNode = gs.getNodeAt(mouseX, mouseY, 0, 16.0f);
  } else {
    startNode = endNode;
  }
}

public void mouseDragged(){
  endNode = gs.getNodeAt(mouseX, mouseY, 0, 16.0f);
}

public void mouseReleased(){
  cursor();
  if(endNode!= null && startNode != null && startNode != endNode){
     start = startNode.id();
     end = endNode.id();
     GraphNode[] p = usePathFinder(pathFinder);
     paths.add(p);
     ready = true; // report that the route is ready
     pathColor.add(color(random(255), random(255), random(255)));
  }
}


